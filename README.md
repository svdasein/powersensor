This is just a recipe for getting a cheap Chinese PZEM-016 power sensor working.  They look like this:

![pzem 016 kit](pzem-016-image.png)*PZEM-016 kit*

If you buy a kit that includes a very very cheap and flimsy RS485-to-USB converter, you're going to want to toss that and get one of these:

![Halfway decent RS485-to-USB converter](rs485toUSBconverter.png)*Halfway decent RS485-to-USB converter*

Wire the bus this way:

![Terminated & powered RS485 bus diagram](RS485bus.png)*Properly powered and terminated RS485 bus*

I wired the "optional" ground sheild and in fact connected that to the power gnd as well.  This works great.  I tried it without any termination or power - it did not work great.

This repository contains a littel ruby script that'll get values from this thing for you.

