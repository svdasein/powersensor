#!/usr/bin/env ruby
require 'rmodbus'
require 'json'
include ModBus

port = '/dev/ttyUSB0'
uid = 1

RTUClient.connect(port, 9600) do |cl|
  cl.with_slave(uid) do |slave|
		# slave.debug = false
		reading = slave.read_input_register(0,10)
		result = {
			volts: reading[0] * 0.1,
			currentA: (reading[1,2].pack('CC').unpack('S')).first * 0.001,
			powerW: (reading[3,4].pack('CC').unpack('S')).first * 0.1,
			energyWh: reading[5,6].pack('CC').unpack('S').first,
			frequencyHz: reading[7] * 0.1,
			powerFactor: reading[8] * 0.01,
			alarm: reading[9]
		}
		puts result.to_json
  end
end
