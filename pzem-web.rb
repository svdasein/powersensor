#!/usr/bin/env ruby
require 'rmodbus'
require 'json'
require 'sinatra'
include ModBus

port = '/dev/ttyUSB0'
uid = 1

set port: 4501
set environment: :production

get '/' do
	result = nil
	RTUClient.connect(port, 9600) do |cl|
		cl.with_slave(uid) do |slave|
			# slave.debug = false
			reading = slave.read_input_register(0,10)
			result = {
				volts: (reading[0] * 0.1).round(3),
				currentA: ((reading[1,2].pack('CC').unpack('S')).first * 0.001).round(3),
				powerW: ((reading[3,4].pack('CC').unpack('S')).first * 0.1).round(3),
				energyWh: (reading[5,6].pack('CC').unpack('S').first).round(3),
				frequencyHz: (reading[7] * 0.1).round(3),
				powerFactor: (reading[8] * 0.01).round(3),
				alarm: reading[9]
			}
		end
	end
	result.to_json
end
